using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateItem : MonoBehaviour
{
    private void Start()
    {
        // Crear un objeto Item usando el constructor sin parámetros
        Item item1 = new Item();
        Debug.Log("Item 1 - Name: " + item1.GetName() + ", Price: $" + item1.GetPrice());

        // Crear un objeto Item usando el constructor con parámetros
        Item item2 = new Item("Espada Mágica", 50.0f);
        Debug.Log("Item 2 - Name: " + item2.GetName() + ", Price: $" + item2.GetPrice());
    }
}
