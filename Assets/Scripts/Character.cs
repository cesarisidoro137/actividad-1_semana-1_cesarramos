using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{
    public Vector2 position;

    public Character(Vector2 startPos)
    {
        position = startPos;
    }

    public abstract void Update();

    public virtual void Dialogue()
    {

    }
}
