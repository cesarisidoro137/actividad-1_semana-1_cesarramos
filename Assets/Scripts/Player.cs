using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character, IDamageable
{
    [SerializeField] private int health;

    public Player(Vector2 startPos, int initialHealth) : base(startPos)
    {
        health = initialHealth;
    }

    public override void Update()
    {
        if (health <= 0)
        {
            Debug.Log("Perdiste");
        }
        else
        {
            //Si no perdi�, puede disparar 
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Shoot();
            }
        }
    }
    //Aplicaci�n de polimorfismo
    public override void Dialogue()
    {
        Debug.Log("Dialogo random del protagonista");
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
    }

    private void Shoot()
    {
        Debug.Log("Disparo");
    }
}
