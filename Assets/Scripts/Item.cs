using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    private string name;
    private float price;

    // Constructor sin parámetros
    public Item()
    {
        name = "Poción genérica";
        price = 2.5f;
    }

    // Constructor con parámetros
    public Item(string itemName, float itemPrice)
    {
        name = itemName;
        price = itemPrice;
    }

    public string GetName()
    {
        return name;
    }

    public float GetPrice()
    {
        return price;
    }
}