using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    //Lista
    private List<Character> gameObjects = new List<Character>();
    //Diccionario
    private Dictionary<string, int> scoreDictionary = new Dictionary<string, int>();

    private void Start()
    {
        // Crear objetos y agregarlos a la lista
        Player player = new Player(new Vector2(0, 0), 100);
        gameObjects.Add(player);
        //LLamado a m�todo di�logo 
        player.Dialogue();

        Enemy enemy1 = new Enemy(new Vector2(5, 0));
        gameObjects.Add(enemy1);
        //LLamado a m�todo di�logo 
        enemy1.Dialogue();

        Enemy enemy2 = new Enemy(new Vector2(-5, 0));
        gameObjects.Add(enemy2);
        //LLamado a m�todo di�logo 
        enemy2.Dialogue();

        // Switch
        foreach (var gameObject in gameObjects)
        {
            switch (gameObject)
            {
                case Player playerObj:
                    playerObj.Update();
                    break;
                case Enemy enemyObj:
                    enemyObj.Update();
                    break;
            }
        }

        // For para realizar alguna operaci�n en la lista
        for (int i = 0; i < gameObjects.Count; i++)
        {
            // Realizar alguna operaci�n
        }

        // While
        int counter = 0;
        while (counter < 10)
        {
            // Acci�n que se repetir� mientras dure el bucle
            counter++;
        }
        //Se llama al m�todo respectivo y se muestra en consola el arreglo de cada posici�n
        Vector2[] enemyPositions = GetAllEnemyPositions();
        foreach (var position in enemyPositions)
        {
            Debug.Log("Posici�n de enemigo: " + position);
        }

    }
    //Arreglo para obtener las posiciones de los enemigos
    private Vector2[] GetAllEnemyPositions()
    {
        List<Vector2> enemyPositions = new List<Vector2>();

        foreach (var gameObject in gameObjects)
        {
            if (gameObject is Enemy enemy)
            {
                enemyPositions.Add(enemy.position);
            }
        }

        return enemyPositions.ToArray();
    }
}
